function maxWords(sentences) {
    let max = -1;
    sentences.forEach((sentence) => {
        let amountOfWords = sentence.split(' ').length;
        if (amountOfWords > max) {
            max = amountOfWords;
        }
    }); 

    return max;
}