function customReduce(items, func, initialValue) {
    let accumulator = initialValue;

    items.forEach(item => {
        accumulator = func(accumulator, item);
    })

    return accumulator;
}